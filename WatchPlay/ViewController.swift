//
//  ViewController.swift
//  WatchPlay
//
//  Created by Anisio Marques Junior on 8/20/16.
//  Copyright © 2016 Anisio Marques Junior. All rights reserved.
//

import UIKit
import Foundation


class ViewController: UIViewController {
    let baseURL = NSURL(string: "http://localhost:3412")!
    var data: NSMutableData = NSMutableData()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendRequest(){
        let todoEndpoint: String = "http://localhost:3412"
        guard let url = NSURL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                print(responseData)
                guard let todo = try NSJSONSerialization.JSONObjectWithData(responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    return
                }
                // now we have the todo, let's just print it to prove we can access it
                print("The todo is: " + todo.description)
                
                // the todo object is a dictionary
                // so we just access the title using the "title" key
                // so check for a title and print it if we have one
                guard let todoTitle = todo["jan"] as? Float else {
                    print("Could not get todo title from JSON")
                    return
                }
                print("The title is: \(todoTitle)")
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
 

}


//
//  WebService.swift
//  WatchPlay
//
//  Created by Anisio Marques Junior on 8/20/16.
//  Copyright © 2016 Anisio Marques Junior. All rights reserved.
//

import UIKit
import Foundation

class WebService {
    let rootURL: NSURL
    let session: NSURLSession

    init(rootURL: NSURL){
        self.rootURL = rootURL;
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        session = NSURLSession(configuration: configuration)
    }
    
    func requestWithURLString(url: String) -> NSURLRequest? {
        if let url = NSURL(string:url) {
            return NSURLRequest(URL: url)
        }
        
        return nil
    }
    
    func executeRequest(requestPath:String) {
        if let request = requestWithURLString(requestPath) {
            print(request)
            let task = session.dataTaskWithRequest(request) { data, response, error in
                print(response)
            }
        }
    }
}

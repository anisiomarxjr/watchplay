//
//  InterfaceController.swift
//  WatchChart Extension
//
//  Created by Anisio Marques Junior on 8/20/16.
//  Copyright © 2016 Anisio Marques Junior. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet weak var chart:WKInterfaceImage!;

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)

        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func update(){
        //let url = "https://chart.googleapis.com/chart?chs=250x150&cht=bvs&chco=4D89F9,C6D9FD&chd=t:10,50,60,80,40&chds=0,80&chbh=a"
        refreshData()

    }
    
    func refreshData(){
        let todoEndpoint: String = "http://localhost:3412"
        guard let url = NSURL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) in

            guard error == nil else {
                print("erro ao chamar GET")
                print(error)
                return
            }

            guard let responseData = data else {
                print("Erro: não recebeu dados")
                return
            }
            

            do {
                var chaves = Array<String>()
                var valores = Array<Float>()

                let json = try NSJSONSerialization.JSONObjectWithData(responseData, options: [])
                
                if let receitas = json["receitas"] as? [[String: AnyObject]] {
                    for receita in receitas {
                        chaves.append(receita["title"] as! String)
                        valores.append(receita["value"] as! Float)
                    }
                    self.generateChartImage(chaves,valores:valores)
                }
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    func generateChartImage(chaves:Array<String>, valores: Array<Float>){        
        let url = buildUrl(chaves, valores: valores)
        
        let encodedUrl = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: encodedUrl!)!) {
            data, response, error in
            if (data != nil && error == nil) {
                let image = UIImage(data: data!, scale: 1.0)
                dispatch_async(dispatch_get_main_queue()) {
                    self.chart.setImage(image)
                }
            }
        }.resume()
    }
    
    func buildUrl(chaves:Array<String>, valores: Array<Float>) -> String {
        let stringChaves = chaves.joinWithSeparator("|")
        var arrayStringValores = Array<String>()
        
        for valor in valores {
            arrayStringValores.append(String(format: "%.2f", valor))
        }
        
        let stringValores = arrayStringValores.joinWithSeparator(",")
        
        //lc ou bvs
        let url = "https://chart.googleapis.com/chart?chs=250x150&cht=bvs&chd=t:\(stringValores)&chxt=x,y&chxl=0:|\(stringChaves)&chxs=0N*e,FFFFFF|1N*cUSD*Mil,FFFFFF&chf=bg,s,000000&chbh=a&chls=5&chg=20,20"
        print(url)
        
        return url
    }
}
